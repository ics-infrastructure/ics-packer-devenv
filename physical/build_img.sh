#!/bin/bash

export CENTOS_ISO=CentOS-7-x86_64-DVD-2003.iso

if [[ ! -f CentOS_7/${CENTOS_ISO} ]]
then
  echo "Downloading the ${CENTOS_ISO} image..."
  curl --fail -o CentOS_7/${CENTOS_ISO} https://artifactory.esss.lu.se/artifactory/swi-pkg/centos/${CENTOS_ISO}
fi

# Create version file for devenv-firstboot
echo "$DEVENV_VERSION" > kickstart/devenv_version

# Create bootstrap script to be run on a clean CentOS install
mkdir -p build
sed "s/^DEVENV_VERSION=.*/DEVENV_VERSION=$DEVENV_VERSION/" kickstart/devenv-firstboot > build/bootstrap-devenv

./create_ks_img
