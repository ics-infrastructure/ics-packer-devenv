Packer template to build ESS DM
===============================

This Packer_ template creates the ESS DM Vagrant_ box.
The repository also contains all the scripts needed to create a bootable USB image
to install the Development machine.

Usage
-----

Images are built automatically by gitlab-ci (you can check the .gitlab-ci.yml).

To test locally you can run the following commands:

1. To build the vagrant box, run::

    $ export DEVENV_VERSION=$(git describe)
    $ packer build devenv-vm.json

   This will create the `build/esss-devenv-7.box` box.

Currently packer no longer support source_path being url and instead needs to be a local path. Packer v1.2.4 still works with an external path/url.

2. To build the USB bootable image, run::

    $ cd physical
    $ ./build_img.sh

   This will create the `physical/build/devenv_ks.<DEVENV_VERSION>.img` image.

Testing
-------

To manually test the vagrant box:

1. Add the box locally::

    $ vagrant box add --name test build/esss-devenv-7.box

2. In an empty directory, run::

    $ vagrant init test
    $ vagrant up

3. To remove the test box::

    $ vagrant destroy
    $ vagrant box remove test


.. _Packer: https://www.packer.io
.. _Vagrant: https://www.vagrantup.com
