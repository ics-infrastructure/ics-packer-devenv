#!/bin/bash

# Exit on any error
set -eux

export PATH=/usr/local/bin:$PATH

# Move uploaded ansible files to /etc
mv /tmp/ansible /etc
chown -R root:root /etc/ansible

ANSIBLE_VERSION=$(cat /etc/ansible/.ansible_version)

# Install ansible-playbook and ansible-galaxy PEX files
for exe in ansible-playbook ansible-galaxy
do
  curl --fail -o /usr/local/bin/${exe} https://artifactory.esss.lu.se/artifactory/swi-pkg/ansible-releases/${ANSIBLE_VERSION}/${exe}
  chmod a+x /usr/local/bin/${exe}
done

# Configure logrotate
cat << EOF > /etc/logrotate.d/ansible
/var/log/ansible.log {
    missingok
    notifempty
    size 100k
    monthly
    create 0666 root root
}
EOF

# Update yum repositories
/usr/local/bin/ansible-playbook /etc/ansible/repo.yml

# Update all packages
yum update -y
# Do NOT run any yum command before the yum repositories have been changed
# to point to mirrors on Artifactory! This might result in conflicts in package versions.
# Install VirtualBox Guest Additions
mkdir -p /mnt
mount -o loop /home/vagrant/VBoxGuestAdditions.iso /mnt
/mnt/VBoxLinuxAdditions.run
umount /mnt

rm /home/vagrant/VBoxGuestAdditions.iso
yum clean all

echo ${DEVENV_VERSION} > /etc/ansible/.devenv_version

ansible-playbook /etc/ansible/devenv.yml

# Change vagrant password
usermod -p "Q5rr2yuXGmHKw" vagrant
