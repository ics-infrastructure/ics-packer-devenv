#!/bin/bash

set -e

echo "Running yum update"
yum -y update

echo "Running ansible playbook"
/usr/local/bin/ansible-playbook /etc/ansible/devenv.yml
