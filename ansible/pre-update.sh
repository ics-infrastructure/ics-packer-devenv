#!/bin/bash

# Script run from devenv-config menu when performing an update of the DM
# It's packaged in the ics-ans-devenv-${DEVENV_VERSION}.tar.gz archive
# and allows to perform extra actions before to run the Ansible plyabook
# It's run as root

# Prevent to upgrade from DM 3.x to 4.x
previous_version=$(cat /etc/ansible/.devenv_version 2>/dev/null)
major=${previous_version:0:1}
if [[ ("$major" != "") && ("$major" < 4) ]]
then
  echo "Upgrading from 3.x to 4.x isn't possible. Aborting."
  exit 2
fi

# Remove old "all" group_vars
# Group changed to "devenv"
rm -f /etc/ansible/group_vars/all

# Install required version of ansible-playbook and ansible-galaxy PEX files
ANSIBLE_VERSION=$(cat /etc/ansible/.ansible_version)
for exe in ansible-playbook ansible-galaxy
do
  curl --fail -o /usr/local/bin/${exe} https://artifactory.esss.lu.se/artifactory/swi-pkg/ansible-releases/${ANSIBLE_VERSION}/${exe}
  chmod a+x /usr/local/bin/${exe}
done

# Update yum repositories
/usr/local/bin/ansible-playbook /etc/ansible/repo.yml

# Update all packages
yum update -y
exit 0
